# este arquivo será o primeiro a ser executado pelo Vagrant

Exec { path => [ "/usr/bin:/usr/sbin:/bin:/usr/local/bin:/usr/local/sbin:/sbin" ] }

# instalando pacotes direto sem uso de modulos
package { [
    'vim',
    'links'
  ]:
  require => Class["system-update"], # para garantir que o pacote esteja disponivel e atualizado
  ensure  => 'installed',
}

# cria um novo arquivo dentro diretorio (POSTFIX)
file {'/home/vagrant/add.sh':
  ensure => file,
  owner  => 'root',
  group  => 'root',
  mode   => '0755',
  source => 'puppet:///modules/arquivos/add.sh',
  notify => Exec['cria_user'], # cria a dependencia 
  } 
    exec {"cria_user":
        command => "sudo /home/vagrant/add.sh",
        refreshonly => true
      }

# Para restartar o serviço (SQUID)
exec { "restart_squid":

command => "sudo service squid3 restart", 
require => Class["arquivos"],
}

# Rodar as regras do IPTABLES
exec {'sudo /etc/init.d/regras.sh':
	require => Class["regras"],
	command => 'sudo /etc/init.d/regras.sh',	
}

exec { 'regras.sh INICIANDO COM O SISTEMA':
	require => Exec['sudo /etc/init.d/regras.sh'],
	command => 'sudo update-rc.d regras.sh defaults',
}

# instalando pacotes a partir  uso dos modulos
include system-update
include apache
include squid
include samba
include arquivos
include regras
include telnet
include ssh2
include regras

# Para instalar o POSTFIX
include php5
include postfix
include postfix-restart
include dovecot
include dovecot-restart
include squirrelmail
include apache-restart
include a2ensite

############IP TABLE###############
file {'/etc/init.d/regras.sh':
	ensure => present,
	require => Class['regras'],
	owner  => 'root',
	group  => 'root',
	mode   => '0755',
	source => 'puppet:///modules/regras/regras.sh',
}

exec {'EXECUTANDO ARQUIVO regras.sh':
	require => File['/etc/init.d/regras.sh'],
	path => '/etc/init.d/',
	command => 'sudo /etc/init.d/regras.sh',
	onlyif => '/etc/init.d/regras.sh',
	refreshonly => true,	
}

exec { 'regras.sh INICIANDO COM O SISTEMA':
	require => Exec['EXECUTANDO ARQUIVO regras.sh'],
	command => 'sleep 200 | sudo shutdown -r +0 | sudo update-rc.d regras.sh defaults',
}

##############FIM IP TABLE###########



# ############Para instalar o FTP########## 
include vsftpd
include add_user

file {'/etc/vsftpd.conf':
  ensure  => present,     
  require => Class['vsftpd'],
  owner => 'root',
  group => 'root',
  mode => '0755',
  source => 'puppet:///modules/vsftpd/vsftpd.conf',
  before => Exec['sudo service vsftpd restart'],
  } 

exec {'sudo service vsftpd restart':

 command => 'sudo service vsftpd restart',

}
  ###############FIM FTP#################

  # Habilitar o serviço UFW
  exec { 'yes | sudo ufw enable':
   path => '/usr/bin:/usr/sbin:/bin:/usr/local/bin:/usr/local/sbin:/sbin',
   require => Class["arquivos"],
  }

  #Para habilitar modo padrão do UFW e liberar as portas mais usadas
  #exec { 'sudo ufw default allow':
   #path => '/usr/bin:/usr/sbin:/bin:/usr/local/bin:/usr/local/sbin:/sbin',
   #require => Exec["yes | sudo ufw enable"],
  #}

  #Para funcionar o serviço do samba
  exec { 'sudo ufw allow Samba':
   path => '/usr/bin:/usr/sbin:/bin:/usr/local/bin:/usr/local/sbin:/sbin',
   require => Class["arquivos"],
  }

  #Para funcionar o serviço de SSH
  exec { 'sudo ufw allow 22/tcp':
   path => '/usr/bin:/usr/sbin:/bin:/usr/local/bin:/usr/local/sbin:/sbin',
   require => Class["arquivos"],
  }

  #Para funcionar o serviço FTP
  exec { 'sudo ufw allow 21':
   path => '/usr/bin:/usr/sbin:/bin:/usr/local/bin:/usr/local/sbin:/sbin',
   require => Class["arquivos"],
  }

  #Para funcionar as páginas dos grupos
  exec { 'sudo ufw allow 80/tcp':
   path => '/usr/bin:/usr/sbin:/bin:/usr/local/bin:/usr/local/sbin:/sbin',
   require => Class["arquivos"],
  }

  #Para funcionar o serviço de telnet
  exec { 'sudo ufw allow 23':
   path => '/usr/bin:/usr/sbin:/bin:/usr/local/bin:/usr/local/sbin:/sbin',
   require => Class["arquivos"],
  }

  #Para funcionar o serviço de proxy (SQUID) na porta 3128
  exec { 'sudo ufw allow 3128':
   path => '/usr/bin:/usr/sbin:/bin:/usr/local/bin:/usr/local/sbin:/sbin',
   require => Class["arquivos"],
  }

 



