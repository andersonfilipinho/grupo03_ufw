class squirrelmail {

#Instala os pacotes do Squirrelmail, garantindo que o Apache2 já esteja instalado, e que o serviço do MySQL esteja rodando.
package { ['squirrelmail']:
ensure => installed,
require => Class['apache'],
notify => Service['php5-fpm']
}

#Envia o arquivo de configuração do Squirrelmail a partir de uma template pré-configurado

file { '/etc/squirrelmail/config.php':
source => 'puppet:///modules/arquivos/config.php',
owner   => 'root',
group   => 'root',
mode    => 644,
require => Package['squirrelmail'],
} 


#Cria um link da pasta do Roundcube para a pasta do servidor web.
exec { 'config-apache2':
command => 'sudo cp /etc/squirrelmail/apache.conf /etc/apache2/sites-available/squirrelmail.conf',
require => Package['squirrelmail']
}


}
