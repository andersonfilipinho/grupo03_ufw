class apache {

  package { "apache2":   # deve conter o nome do pacote a ser instalado
    ensure  => present,
    require => Class["system-update"],
  }

  service { "apache2":    # nome do serviço 
    ensure  => "running",
    require => Package["apache2"],
  }

}