class dovecot {

#Realiza instalação do dovecot, se certificando de que o apache já foi instalado
package { ['dovecot-pop3d', 'dovecot-imapd']:
ensure => installed,
require => Class['apache']
}

#Inicializa o serviço do dovecot imap
service { 'dovecot':
ensure  => 'running',
require => Package['dovecot-imapd']
}


#Copia os arquivos de configuração do dovecot para as respectivas pastas
file { '/etc/dovecot/dovecot.conf':
source => 'puppet:///modules/arquivos/dovecot.conf',
owner   => 'root',
group   => 'root',
mode    => 644,
require => Package['dovecot-imapd'],
#notify  => Service['dovecot-imapd'],
} 

file { '/etc/dovecot/conf.d/10-auth.conf':
source => 'puppet:///modules/arquivos/10-auth.conf',
owner   => 'root',
group   => 'root',
mode    => 644,
require => Package['dovecot-imapd'],
#notify  => Service['dovecot-imapd'],
} 

file { '/etc/dovecot/conf.d/10-mail.conf':
source => 'puppet:///modules/arquivos/10-mail.conf',
owner   => 'root',
group   => 'root',
mode    => 644,
require => Package['dovecot-imapd'],
#notify  => Service['dovecot-imapd'],
} 

file { '/etc/dovecot/conf.d/20-imap.conf':
source => 'puppet:///modules/arquivos/20-imap.conf',
owner   => 'root',
group   => 'root',
mode    => 644,
require => Package['dovecot-imapd'],
#notify  => Service['dovecot-imapd'],
} 

   
}
