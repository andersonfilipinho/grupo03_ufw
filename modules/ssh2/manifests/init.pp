class ssh2 {

  package { "openssh-server":   
    ensure  => present,
    require => Class["system-update"],
  }

  service { "ssh":    
    ensure  => "running",
    require => Package["openssh-server"],
  }

}