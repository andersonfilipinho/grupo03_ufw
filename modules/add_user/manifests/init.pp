class add_user {

  user { 'user1':
    ensure => 'present',
    comment => 'User 1',
    managehome => true,

  }

  user { 'user2':
    ensure => 'present',
    comment => 'User 2',
    managehome => true,
  
  }
}