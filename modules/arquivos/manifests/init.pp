class arquivos {

# Cria um arquivo index.html no diretório var/www baseado no arquivo local index.txt, que é a página do grupo
file {'/var/www/index.html':
ensure => file,
	owner  => 'root',
	group  => 'root',
	mode   => '0644',
	source => 'puppet:///modules/arquivos/index.txt',
	require => Class["apache"];
} 

# Criar diretório para o CSS
file {'/var/www/css':
ensure => directory,
	owner  => 'root',
	group  => 'root',
	mode   => '0644',
	require => Class["apache"];
}

# Criar diretório para as imagens
file {'/var/www/img':
ensure => directory,
	owner  => 'root',
	group  => 'root',
	mode   => '0644',
	require => Class["apache"];
}

# Criar diretório para as páginas auxiliares
file {'/var/www/html':
ensure => directory,
	owner  => 'root',
	group  => 'root',
	mode   => '0644',
	require => Class["apache"];
}

# copiar arquivos css para o novo diretório
	file {'/var/www/css/bootstrap.css':
	ensure => file,
	require => File['/var/www/css'],  # para garantir que existe o diretorio preste atenção na sintaxe com F mauisculo e o uso de []
	owner  => 'root',
	group  => 'root',
	mode   => '0644',
	source => 'puppet:///modules/arquivos/bootstrap.css';
	}

	file {'/var/www/css/bootstrap.min.css':
	ensure => file,
	  require => File['/var/www/css'],  
	  owner  => 'root',
	  group  => 'root',
	  mode   => '0644',
	  source => 'puppet:///modules/arquivos/bootstrap.min.css';
	}    

	file {'/var/www/css/custom.css':
	ensure => file,
	  require => File['/var/www/css'],  
	  owner  => 'root',
	  group  => 'root',
	  mode   => '0644',
	  source => 'puppet:///modules/arquivos/custom.css';
	}

#copiar logo da computação para a VM no diretório img
	file {'/var/www/img/computacao.png':
	ensure => file,
	  require => File['/var/www/img'],  
	  owner  => 'root',
	  group  => 'root',
	  mode   => '0644',
	  source => 'puppet:///modules/arquivos/computacao.png';
	}	    	       

#Copiar as páginas auxiliares da homepage para o diretório var/www/html
file {'/var/www/html/apachexnginx.html':
	ensure => file,
	  require => File['/var/www/html'],  
	  owner  => 'root',
	  group  => 'root',
	  mode   => '0644',
	  source => 'puppet:///modules/arquivos/apachexnginx.txt';
	}

file {'/var/www/html/instalacao.html':
	ensure => file,
	  require => File['/var/www/html'],  
	  owner  => 'root',
	  group  => 'root',
	  mode   => '0644',
	  source => 'puppet:///modules/arquivos/instalacao.txt';
	}

#comandos para funcionar o SQUID
file {'/etc/squid3/squid.conf':
     ensure => file,
     require => Class["squid"],
     owner  => 'root',
     group  => 'root',
     mode   => '0644',
     source => 'puppet:///modules/arquivos/squid.conf',
   }

file {'/etc/squid3/not_allowed.acl':
     ensure => file,
     require => Class["squid"],
     owner  => 'root',
     group  => 'root',
     mode   => '0644',
     source => 'puppet:///modules/arquivos/not_allowed.acl',
   }		    
}

