

#Realiza o download e instalação do pacote php5
class php5 {
package { "php5-fpm": 
require => Class["system-update"], # para garantir que o pacote esteja disponivel e atualizado
ensure  => installed,
}

#Garante que o serviço do php estará rodando após a instalação do pacote
service { "php5-fpm":
ensure  => "running",
require => Package["php5-fpm"],
}

}
